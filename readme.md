# SASS-sources for responsive checkout

SASS-sources to build the file ``baseStyleResponsiveCheckout.css``. Includes grunt.

## Install instructions

Install Node to get npm: [http://nodejs.org](http://nodejs.org/)

Install Git: [http://git-scm.com/download/](http://git-scm.com/download/)

Clone repo from BitBucket

Start terminal, cd to repo-folder containing this file

Run ``sudo npm install`` in terminal

Run ``sudo npm install -g grunt`` in terminal

Run ``sudo npm install -g grunt-cli`` in terminal

Run ``grunt`` in terminal

Change the output path ('files') in grunt-task 'sass'

Done.

## Usage for developers

Run ``grunt develop`` in terminal, this will setup watchers. Edit SASS-sources and CSS will be generated and updated automatically.


## TODO

Move all media queries into each include file, keeping them in a separate file is messy.