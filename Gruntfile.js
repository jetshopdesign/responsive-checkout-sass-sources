module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        sass: {
            options: {
                includePaths: ['scss'],
                noCache: true
            },
            develop: {
                options: {
                    debugInfo: true,
                    trace: true,
                    lineNumbers: true,
                    outputStyle: 'nested',
                    sourceMap: '../commerce_main/StoreJ3/SystemCss/baseStyleResponsiveCheckout.css.map'
                },
                files: {
                    '../commerce_main/StoreJ3/SystemCss/baseStyleResponsiveCheckout.css': 'responsive-checkout/responsive-checkout.scss',
                    '../commerce_main/StoreJ3/SystemCss/baseStyleResponsiveCheckoutMediaQueries.css': 'responsive-checkout/responsive-checkout-media-queries.scss'
                }
            },
            production: {
                options: {
                    outputStyle: 'nested'
                },
                files: {
                    '../commerce_main/StoreJ3/SystemCss/baseStyleResponsiveCheckout.css': 'responsive-checkout/responsive-checkout.scss',
                    '../commerce_main/StoreJ3/SystemCss/baseStyleResponsiveCheckoutMediaQueries.css': 'responsive-checkout/responsive-checkout-media-queries.scss'
                }
            }
        },
        watch: {
            production: {
                files: 'responsive-checkout/*.scss',
                tasks: ['sass:production']
            },
            develop: {
                files: 'responsive-checkout/*.scss',
                tasks: ['sass:develop']
            }
        }
    });

    // Load all libs
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');


    grunt.registerTask('default', ['sass:production', 'watch:production']);
    grunt.registerTask('develop', ['sass:develop', 'watch:develop']);
    grunt.registerTask('production', ['sass:production']);
};